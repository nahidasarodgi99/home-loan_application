package com.cg.application.service;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.cg.application.entity.Customer;


@ExtendWith({SpringExtension.class})
@DataJpaTest
@Import(ICustomerServiceImp.class)
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class CustomerServiceTest {
	@Autowired
	private ICustomerServiceImp service;
	@Autowired
	private EntityManager em;
	
	@Test
	public void testFindAll() {
		List<Customer> custFound=service.findAll();
		System.out.println("All student"+custFound);
		Assertions.assertNotNull(custFound);
	}
	@Test
	public void testFindById() {
		Customer cust = new Customer("nikita","9876453276","nikita@gmail.com",null, "female","india","997645327865","9876474532");
		em.persist(cust);
		Integer id = cust.getCustId();
		Customer custFound = service.findById(86);//id);
		System.out.println(custFound);
		Assertions.assertEquals(custFound.getCustomerName(),cust.getCustomerName());
	}
	
	@Test
	public void testRegister() {
		Customer cust=service.register(new Customer("nikita","9876453276","nikita@gmail.com",null, "female","india","997645327865","9876474532"));
		Integer custid = cust.getCustId();
		Customer custFound = service.findById(custid);
		System.out.println(custFound);
		Assertions.assertEquals(custFound.getCustomerName(), cust.getCustomerName());
		
	}
	
	

}
