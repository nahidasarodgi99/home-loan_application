package com.cg.application.service;
import javax.persistence.EntityManager;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.cg.application.entity.UserDetails;


@ExtendWith({SpringExtension.class})
@DataJpaTest
@Import(CustomerRegisterServiceImpl.class)
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class CustomerRegisterServiceTest {
	
	@Autowired
	private CustomerRegisterServiceImpl service;
	@Autowired
	private EntityManager em;
	
	@Test
	public void testuserregister() {
		UserDetails ust=service.userregister(new UserDetails("jam","jam123","User"));
		String name= ust.getUsername();
		UserDetails userFound = service.findByName(name);
		System.out.println(userFound);
		Assertions.assertEquals(userFound.getUsername(), ust.getUsername());
		
	}
	
	

}
