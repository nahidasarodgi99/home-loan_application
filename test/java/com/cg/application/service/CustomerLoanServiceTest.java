package com.cg.application.service;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.cg.application.entity.Loan;


@ExtendWith({SpringExtension.class})
@DataJpaTest
@Import(CustomerLoanServiceImpl.class)
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class CustomerLoanServiceTest {
	
	@Autowired
	private CustomerLoanServiceImpl service;
	@Autowired
	private EntityManager em;
	
	@Test
	public void testFindByCustId() {
		List<Loan> loan = service.findByCustId(86);
		System.out.println(loan);
		Assertions.assertTrue(loan.size()>0);
	}
	
	@Test
	public void testFindByCustIdAppId() {
		Loan loan = new Loan(88,100000,86);
		Loan loanFound = service.findByCustIdAppId(86, 88);
		System.out.println(loanFound);
		Assertions.assertEquals(loanFound.getApplyamount(),loan.getApplyamount());
	}
}
