package com.cg.application.controller;
import com.cg.application.util.LoanUtil;


import java.sql.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.http.HttpStatus;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.cg.application.dto.CreateCustomerRequest;
import com.cg.application.dto.LoanDetails;
import com.cg.application.dto.UserDetailDto;
import com.cg.application.entity.Customer;
import com.cg.application.entity.Emi;
import com.cg.application.entity.Loan;
import com.cg.application.entity.LoanTracker;
import com.cg.application.entity.UserDetails;
import com.cg.application.exception.AlreadyRegisterException;
import com.cg.application.exception.CustomerApprovedException;
import com.cg.application.exception.CustomerNotFoundException;
import com.cg.application.exception.LandOfficerFinanceofficerNotApprovedException;
import com.cg.application.exception.LoginRequiredException;
import com.cg.application.exception.NotAccessibleException;
import com.cg.application.service.ICustomerLoanService;
import com.cg.application.service.ICustomerRegister;
import com.cg.application.service.ICustomerService;
import com.cg.application.util.CustomerUtil;


@RestController
@RequestMapping("/customer")
@Validated
public class CustomerController {
	@Autowired
	private ICustomerService service;
	@Autowired
	private CustomerUtil custUtil;
	@Autowired
	private ICustomerRegister cRegister;
	@Autowired
	private LoanUtil loanUtil;
	@Autowired
	private ICustomerLoanService loanService;
	
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping("/add")
	public Customer addPersonalDetails(@RequestBody @Valid CreateCustomerRequest requestData,
			HttpServletRequest request) {
		HttpSession session = request.getSession();
		
		if (session.getAttribute("username") == null) {
			throw new NotAccessibleException("Plz login First");
		}
		if (!session.getAttribute("role").equals("User")) {
			throw new NotAccessibleException("Cannot be accessed by : " + session.getAttribute("role"));
		}
		Customer customer = new Customer(requestData.getCustomerName(), requestData.getMobileNumber(), requestData.getEmail(),
				requestData.getDateOfBirth(), requestData.getGender(), requestData.getNationality(),
				requestData.getAadharNumber(), requestData.getPanNumber());
		customer = service.register(customer);
		String uName = (String) session.getAttribute("username");
		customer.setUsername(uName);
		Customer details = custUtil.toDetails(customer);
		System.out.println(uName);
		return details;
	}
	
	@GetMapping("/by/id/{id}")
	public Customer findCustomerById(@PathVariable("id") int id, HttpServletRequest request) {
		HttpSession session = request.getSession();
		if (session.getAttribute("username") == null) {
			throw new LoginRequiredException("Please login first");
		}
		if (!session.getAttribute("role").equals("Admin")) {
			throw new NotAccessibleException("Cannot be accessed by : " + session.getAttribute("role"));
		}
		Customer customer = service.findById(id);
		Customer details = custUtil.toDetails(customer);
		return details;
	}
	@GetMapping("/allcustomers")
	public List<Customer> findall(HttpServletRequest request) {
		HttpSession session = request.getSession();
		if (session.getAttribute("username") == null) {
			throw new LoginRequiredException("Please login first");
		}
		if (!session.getAttribute("role").equals("Admin")) {
			throw new NotAccessibleException("Cannot be accessed by : " + session.getAttribute("role"));
		}
		List<Customer> customers = service.findAll();
		List<Customer> details = custUtil.toDetails(customers);
		return details;
	}
	
	@GetMapping("/loan/Apply/{date}/{applyamount}/{custid}")
	public LoanDetails addLoan(@PathVariable("custid") int custid,@PathVariable("date") Date date, @PathVariable("applyamount") float amount,
			HttpServletRequest request) {
		HttpSession session = request.getSession();
		if (session.getAttribute("username") == null) {
			throw new LoginRequiredException("Please login first");
		}
		if (!session.getAttribute("role").equals("User")) {
			throw new NotAccessibleException("Cannot be accessed by : " + session.getAttribute("role"));
		}
		Loan loan = new Loan(date, amount, custid, "Not Approved");
		loan = loanService.register(loan);
		LoanDetails details = loanUtil.toDetails(loan);
		return details;
	}
	
	@GetMapping("/loan/removeLoanByCustomerId/{custid}/{appid}")
	public LoanDetails deleteLoan(@PathVariable("custid") int custid,@PathVariable("appid") int appid, HttpServletRequest request) {
		HttpSession session = request.getSession();
		if (session.getAttribute("username") == null) {
			throw new LoginRequiredException("Please login first");
		}
		if (!session.getAttribute("role").equals("User")) {
			throw new NotAccessibleException("Cannot be accessed by : " + session.getAttribute("role"));
		}
		Loan loan = loanService.findByCustIdAppId(custid, appid);
		if (loan.getStatus().equals("Approved") || loan.getStatus().equals("Rejected")) {
			throw new CustomerApprovedException("Cannot delete approved/rejected loans");
		}
		LoanDetails details = loanUtil.toDetails(loan);
		loanService.remove(loan.getApplicationid());
		return details;
	}
	
	
	@GetMapping("/loan/getAllLoansAppliedByCustomerId/{custid}")
	public List<LoanDetails> getAllLoansByCustomerId(@PathVariable("custid") int custid,HttpServletRequest request) {
		HttpSession session = request.getSession();
		if (session.getAttribute("username") == null) {
			throw new LoginRequiredException("Please login first");
		}
		if (!session.getAttribute("role").equals("User")) {
			throw new NotAccessibleException("Cannot be accessed by : " + session.getAttribute("role"));
		}
		
		List<Loan> loans = loanService.findByCustId(custid);
		List<LoanDetails> details = loanUtil.toDetails(loans);
		return details;
	}
	
	@GetMapping("/loan/loanTracker/{custid}/{applicationid}")
	public LoanTracker getLoanDetails(@PathVariable("custid") int custid,@PathVariable("applicationid") int appid, HttpServletRequest request) {
		HttpSession session = request.getSession();
		if (session.getAttribute("username") == null) {
			throw new LoginRequiredException("Please login first");
		}
		
		Loan loan = loanService.findByCustIdAppId(custid, appid);
		LoanTracker lt = new LoanTracker();

		lt = loanService.loanTracker(loan);
		return lt;
	}
	
	//land officer
	@GetMapping("/loan/updateLandDocuments/{customerid}/{applicationid}/{landdocuments}")
	public LoanDetails updateLoanLandDocuments(@PathVariable("customerid") int id,@PathVariable("applicationid") int appid, @PathVariable("landdocuments") boolean landdocumentsupdate,HttpServletRequest request) {
		HttpSession session = request.getSession();
		if (session.getAttribute("username") == null) {
			throw new LoginRequiredException("Please login first");
		}
		if (!session.getAttribute("role").equals("landofficer")) {
			throw new NotAccessibleException("Cannot be accessed by : " + session.getAttribute("role"));
		}
		System.out.println("Controller fetch Customer id: " + id + " with Application id:" + appid);
		Loan loan = loanService.findByCustIdAppId(id, appid);
		if(loan.getStatus().equals("Approved") || loan.getStatus().equals("Rejected")) {
			throw new CustomerApprovedException("Already Updated....... Can't update again");
		}else {
			loan.setLandverify(landdocumentsupdate);
			loan = loanService.register(loan);
		}
		LoanDetails details = loanUtil.toDetails(loan);
		return details;
	}
	
	//AdminApproval
	@GetMapping("/loan/UpdateAdminApproval/{customerid}/{applicationid}/{adminapprove}")
	public LoanDetails updateLoanAdmin(@PathVariable("customerid") int id,@PathVariable("applicationid") int appid, @PathVariable("adminapprove") boolean adminapprove,HttpServletRequest request) throws LandOfficerFinanceofficerNotApprovedException {
		HttpSession session = request.getSession();
		if (session.getAttribute("username") == null) {
			throw new LoginRequiredException("Please login first");
		}
		if (!session.getAttribute("role").equals("Admin")) {
			throw new NotAccessibleException("Cannot be accessed by : " + session.getAttribute("role"));
		}
		System.out.println("Controller fetch Customer id: " + id + " with Application id:" + appid);
		Loan loan = loanService.findByCustIdAppId(id, appid);
		if(!(loan.isFinanceverify() && loan.isLandverify()))
		{
			throw new LandOfficerFinanceofficerNotApprovedException("Cannot Update  please check finance and land approval status....... ");
		}
		else
		{
		if(loan.getStatus().equals("Approved") || loan.getStatus().equals("Rejected") ) {
			throw new CustomerApprovedException("Already Updated....... Can't update again");
		}
		
		else {
			loan.setAdminapprove(adminapprove);
			loan.setStatus("Approved");
			loan = loanService.register(loan);
		}
		LoanDetails details = loanUtil.toDetails(loan);
		return details;
		}
	}
	
	//FinanceApproval
	@GetMapping("/loan/updateFinanceDocuments/{customerid}/{applicationid}/{finance}/{approveamount}")
	public LoanDetails updateLoanFinance(@PathVariable("customerid") int id,@PathVariable("applicationid") int appid, @PathVariable("finance") boolean financeupdate ,@PathVariable("approveamount") float approveamount,HttpServletRequest request) {
		System.out.println("Controller fetch Customer id: " + id + " with Application id:" + appid);
		HttpSession session = request.getSession();
		if (session.getAttribute("username") == null) {
			throw new LoginRequiredException("Please login first");
		}
		if (!session.getAttribute("role").equals("financeofficer")) {
			throw new NotAccessibleException("Cannot be accessed by : " + session.getAttribute("role"));
		}
		
		Loan loan = loanService.findByCustIdAppId(id, appid);
		if(loan.getStatus().equals("Approved") || loan.getStatus().equals("Rejected")) {
			throw new CustomerApprovedException("Already Updated....... Can't update again");
		}else {
			loan.setFinanceverify(financeupdate);
			System.out.println("approveamount is"+approveamount);
			loan.setApproveamount(approveamount);
			loan = loanService.register(loan);
		}
		LoanDetails details = loanUtil.toDetails(loan);
		return details;
	}
	
	
	@GetMapping("/emi/simpleEMI/{loanAmount}/{rateOfInterest}/{timePeriod}")
	public Emi calculateEmi(@PathVariable("loanAmount") float loanAmount,
			@PathVariable("rateOfInterest") double rateOfInterest, @PathVariable("timePeriod") int timePeriod) {
		double interest = (double) ((loanAmount * (rateOfInterest * 0.01)) / timePeriod);
		double emiAmount = ((loanAmount / timePeriod) + interest);
		double totalEmiAmount = interest * timePeriod;
		double totalAmount = emiAmount * timePeriod;
		Emi emi = new Emi(loanAmount, rateOfInterest, timePeriod, interest, emiAmount, totalEmiAmount, totalAmount);
		return emi;
	}
	@PutMapping("/updateCustomer")
	public Customer updateCustomer(@RequestBody Customer customer)throws CustomerNotFoundException{
		return this.service.updateCustomer(customer);
	}

	
	@ResponseStatus(HttpStatus.ACCEPTED)
	@PostMapping("/register")
	public String userregister(@RequestBody UserDetailDto userDetails, HttpServletRequest request) {
		HttpSession session = request.getSession();
		if (session.getAttribute("username") != null) {
			throw new AlreadyRegisterException("Yuor Already Register Not requried to Regsiter");
		}
		UserDetails uDetails = new UserDetails(userDetails.getUsername(), userDetails.getPassword(), "User");
		uDetails = cRegister.userregister(uDetails);
		return "Registration successful with Username : " + uDetails.getUsername() + " Role-> "
				+ uDetails.getUserRole();
	}
	@ResponseStatus(HttpStatus.ACCEPTED)
	@PostMapping("/login")
	public String login(@RequestBody UserDetailDto userDetails, HttpServletRequest request) {
		HttpSession session = request.getSession();
		UserDetails uDetails = cRegister.findByName(userDetails);
		session.setAttribute("username", uDetails.getUsername());
		session.setAttribute("role", uDetails.getUserRole());
		return "Login Successful.......Welcome " + uDetails.getUsername() + " ->" + uDetails.getUserRole();
	}
	@ResponseStatus(HttpStatus.ACCEPTED)
	@PostMapping("/logout")
	public String logout(@RequestBody UserDetailDto userDetails, HttpServletRequest request) {
		HttpSession session = request.getSession();
		if (userDetails.getUsername().equals(session.getAttribute("username"))) {
			session.invalidate();
			return "You have successfully logged out " + userDetails.getUsername();
		}
		return "User Not Login to Logout";
	}

}
