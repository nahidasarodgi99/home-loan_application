package com.cg.application.service;

import java.util.List;

import com.cg.application.entity.Loan;
import com.cg.application.entity.LoanTracker;

public interface ICustomerLoanService {

	Loan register(Loan loan);
	
	List<Loan> findByCustId(int id);
	
	Loan findByCustIdAppId(int id, int appid);
	
	LoanTracker loanTracker(Loan loan);
	
	void remove(int id);

}
