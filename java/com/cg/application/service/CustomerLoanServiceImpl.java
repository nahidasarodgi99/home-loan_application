package com.cg.application.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cg.application.entity.Loan;
import com.cg.application.entity.LoanTracker;
import com.cg.application.exception.CustomerApplicationNotFoundException;
import com.cg.application.exception.CustomerNotFoundException;
import com.cg.application.dao.ICustomerLoanDao;
@Component
public class CustomerLoanServiceImpl implements ICustomerLoanService{
	@Autowired
	private ICustomerLoanDao dao;

	@Override
	public Loan register(Loan loan) {
		loan = dao.save(loan);
		System.out.println(loan);
		return loan;
	}
	
	@Override
	public Loan findByCustIdAppId(int id, int appid) {
		System.out.println("Application loanId"+appid+"Customer id"+id);
		List<Loan> loanList = dao.findByCustId(id);
		if(!(loanList.size()>0)) {
			throw new CustomerNotFoundException("No loans applied by Customer with id : " + id);
		}
		Optional<Loan> opt1 = dao.findByCustIdAppId(id, appid);
		if(!opt1.isPresent()) {
			throw new CustomerApplicationNotFoundException("Application with id : " + appid + " not present for Customer id : " + id);
		}
		Loan loan = opt1.get();
		System.out.println(loan);
		return loan;
	}
	
	@Override
	public List<Loan> findByCustId(int id) {
		List<Loan> loanList = dao.findByCustId(id);
		if(!(loanList.size()>0)) {
			throw new CustomerNotFoundException("No loans applied by Customer with id : " + id);
		}
		return loanList;
	}
	
	@Override
	public LoanTracker loanTracker(Loan loan) {
		
		LoanTracker lt = new LoanTracker();
		
		lt.setStatus(loan.getStatus());
		
		if(loan.isFinanceverify() == false)
			lt.setFinanceApproval("Finance Approval Required");
		else
			lt.setFinanceApproval("Finance Details Approved");
		
		if(loan.isLandverify() == false)
			lt.setLandApproval("Land Approval Required");
		else
			lt.setLandApproval("Land Details Approved");
		
		if(loan.isAdminapprove() == false)
			lt.setAdminApproval("Admin Approval Required");
		else
			lt.setAdminApproval("Admin Approved");
		
		return lt;
	}
	
	
	@Override
	public void remove(int id) {
		dao.deleteById(id);
	}
	
	
}
