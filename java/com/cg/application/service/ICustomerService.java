package com.cg.application.service;

import java.util.List;

import com.cg.application.dto.UserDetailDto;
import com.cg.application.entity.Customer;
import com.cg.application.entity.Loan;


public interface ICustomerService {
	Customer register(Customer customer);

	List<Customer> findAll();
	
	Customer findById(int id);

	Customer updateCustomer(Customer customer);


	
	

}
