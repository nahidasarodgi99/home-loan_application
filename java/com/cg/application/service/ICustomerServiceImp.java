package com.cg.application.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.cg.application.dao.ICustomerDao;
import com.cg.application.entity.Customer;
import com.cg.application.exception.CustomerAlreadyExistsException;
import com.cg.application.exception.CustomerNotFoundException;
import com.cg.application.exception.NoDataToDisplayException;

@Service
@Transactional
public class ICustomerServiceImp implements ICustomerService{
	@Autowired
	private ICustomerDao cDao;

	@Override
	public Customer register(Customer customer) {
		boolean exists = customer.getCustId()==0 && cDao.existsById(customer.getCustId());
        if(exists){
            throw new CustomerAlreadyExistsException("Customer already exists for id="+customer.getCustId());
        }
        customer = cDao.save(customer);
        System.out.println("returning saved stud: " + customer);
        return customer;
	}
	@Override
	public List<Customer> findAll() {
		System.out.println(cDao.getClass().getName());
		List<Customer> list = cDao.findAll();
		if(list.size() < 1) {
			throw new NoDataToDisplayException("No Data");
		}
		return list;
	}
	@Override
	public Customer findById(int id) {
		Optional<Customer> opt = cDao.findById(id);
		if(!opt.isPresent()) {
			System.out.println("Error");
			throw new CustomerNotFoundException("Customer Doesn't exist with id : " + id);
		}
		Customer customer = opt.get();
		System.out.println(customer);
		return customer;
	}
	public Customer updateCustomer(Customer customer) {
		Optional<Customer> optional=cDao.findById(customer.getCustId());
		if(optional.isPresent()) {
			cDao.save(customer);
			return optional.get();
		}
		else {
			throw new CustomerNotFoundException("Customer could not update");
		}
	}


	

}
