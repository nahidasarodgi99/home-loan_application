package com.cg.application.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cg.application.dao.ICustomerRegisterDao;
import com.cg.application.dto.UserDetailDto;
import com.cg.application.entity.UserDetails;
import com.cg.application.exception.CustomerAlreadyExistsException;
import com.cg.application.exception.PasswordAuthenticationFailedException;
import com.cg.application.exception.UserRegistrationException;

@Service
@Transactional
public class CustomerRegisterServiceImpl implements ICustomerRegister{
	@Autowired
	private ICustomerRegisterDao cDao;
	
	@Override
	public UserDetails userregister(UserDetails uDetails) {
		List<UserDetails> uDetailsList = cDao.findAll();
		for (UserDetails user : uDetailsList) {
			if(user.getUsername().equals(uDetails.getUsername())) {
				throw new CustomerAlreadyExistsException("Username : " + uDetails.getUsername() + " alreday exists");
			}
		}
		UserDetails details = cDao.save(uDetails); 
		return details;
	}

	/*@Override
	public UserDetails register1(UserDetails uDetails) {
		UserDetails details = cDao.save(uDetails); 
		return details;
	}*/
	@Override
	public UserDetails findByName(String uName) {
		Optional<UserDetails> opt = cDao.findById(uName);
		return opt.get();
	}

	@Override
	public UserDetails findByName(UserDetailDto details) {
		UserDetails udetails = cDao.findByUsername(details.getUsername());
		if(details==null) {
			throw new UserRegistrationException("User with username : " + details.getUsername() + " not registered");
		}
		if(!details.getPassword().equals(udetails.getPassword())) {
			throw new PasswordAuthenticationFailedException("Invalid credentials");
		}
		return udetails;
	}

	
	
	
	

}
