package com.cg.application.dto;

import java.time.LocalDate;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class CreateCustomerRequest {
	@NotBlank @Size(min=2,max=20)
	private String customerName;
	@NotBlank @Size(min=10,max=12)
	private String mobileNumber;
	private String email;
	private LocalDate dateOfBirth;
	private String gender;
	private String nationality;
	@NotBlank @Size(min=12,max=12)
	private String aadharNumber;
	@NotBlank @Size(min=10,max=10)
	private String panNumber;
	
	public CreateCustomerRequest() {
	}

	public CreateCustomerRequest(String customerName,String mobileNumber, String email, LocalDate dateOfBirth, String gender, String nationality, @NotBlank @Size(min = 12, max = 12) String aadharNumber,String panNumber) {
		super();
		this.customerName = customerName;
		this.mobileNumber = mobileNumber;
		this.email = email;
		this.dateOfBirth = dateOfBirth;
		this.gender = gender;
		this.nationality = nationality;
		this.aadharNumber = aadharNumber;
		this.panNumber = panNumber;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getAadharNumber() {
		return aadharNumber;
	}

	public void setAadharNumber(String aadharNumber) {
		this.aadharNumber = aadharNumber;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	@Override
	public String toString() {
		return "CreateCustomerRequest [customerName=" + customerName + ", mobileNumber=" + mobileNumber + ", email="
				+ email + ", dateOfBirth=" + dateOfBirth + ", gender=" + gender + ", nationality=" + nationality
				+ ", aadharNumber=" + aadharNumber + ", panNumber=" + panNumber + "]";
	}
	
	
	

}
