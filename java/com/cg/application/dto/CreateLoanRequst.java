package com.cg.application.dto;

import java.sql.Date;

public class CreateLoanRequst {
	private int applicationid;
	private Date date;
	private double applyamount;
	private double approveamount;
	private boolean landverify;
	private boolean financeverify;
	private boolean adminapprove;
	private String status;
	private int id;
	public CreateLoanRequst() {
	}
	public CreateLoanRequst(int applicationid, Date date, double applyamount, double approveamount,
			boolean landverify, boolean financeverify, boolean adminapprove, String status, int id) {
		super();
		this.applicationid = applicationid;
		this.date = date;
		this.applyamount = applyamount;
		this.approveamount = approveamount;
		this.landverify = landverify;
		this.financeverify = financeverify;
		this.adminapprove = adminapprove;
		this.status = status;
		this.id = id;
	}
	public int getApplicationid() {
		return applicationid;
	}
	public void setApplicationid(int applicationid) {
		this.applicationid = applicationid;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public double getApplyamount() {
		return applyamount;
	}
	public void setApplyamount(double applyamount) {
		this.applyamount = applyamount;
	}
	public double getApproveamount() {
		return approveamount;
	}
	public void setApproveamount(double approveamount) {
		this.approveamount = approveamount;
	}
	public boolean isLandverify() {
		return landverify;
	}
	public void setLandverify(boolean landverify) {
		this.landverify = landverify;
	}
	public boolean isFinanceverify() {
		return financeverify;
	}
	public void setFinanceverify(boolean financeverify) {
		this.financeverify = financeverify;
	}
	public boolean isAdminapprove() {
		return adminapprove;
	}
	public void setAdminapprove(boolean adminapprove) {
		this.adminapprove = adminapprove;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "CreateLoanRequst [applicationid=" + applicationid + ", date=" + date + ", applyamount=" + applyamount
				+ ", approveamount=" + approveamount + ", landverify=" + landverify + ", financeverify=" + financeverify
				+ ", adminapprove=" + adminapprove + ", status=" + status + ", id=" + id + "]";
	}
	
	

}
