package com.cg.application.exception;

public class UserRegistrationException extends RuntimeException {
	public UserRegistrationException(String msg) {
		super(msg);
	}

}
