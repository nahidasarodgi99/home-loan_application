package com.cg.application.exception;

public class LoanNotApproved extends RuntimeException {
	public LoanNotApproved(String msg) {
		super(msg);
	}

}
