package com.cg.application.exception;

public class NotAccessibleException extends RuntimeException {

	public NotAccessibleException(String msg) {
		super(msg);
	}
	

}
