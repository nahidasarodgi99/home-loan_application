package com.cg.application.exception;

public class CustomerApprovedException extends RuntimeException {
	public CustomerApprovedException(String msg) {
		super(msg);
	}

}
