package com.cg.application.exception;

public class LandOfficerFinanceofficerNotApprovedException extends RuntimeException{
	public LandOfficerFinanceofficerNotApprovedException(String msg) {
		super(msg);
	}

}
