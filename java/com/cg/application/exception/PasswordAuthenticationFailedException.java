package com.cg.application.exception;

public class PasswordAuthenticationFailedException extends RuntimeException {
	public PasswordAuthenticationFailedException(String msg) {
		super(msg);
	}

}
