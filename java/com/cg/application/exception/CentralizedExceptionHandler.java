package com.cg.application.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class CentralizedExceptionHandler {
	
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(CustomerNotFoundException.class)
	public String handleCustomerNotFoundException(CustomerNotFoundException e) {
		return e.getMessage();
	}
	
	@ResponseStatus(HttpStatus.FOUND)
	@ExceptionHandler(CustomerAlreadyExistsException.class)
	public String handleCustomerAlreadyExistsException(CustomerAlreadyExistsException e) {
		return e.getMessage();
	}
	
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(CustomerApplicationNotFoundException.class)
	public String handleCustomerApplicationNotFoundException(CustomerApplicationNotFoundException e) {
		return e.getMessage();
	}
	
	@ResponseStatus(HttpStatus.NON_AUTHORITATIVE_INFORMATION)
	@ExceptionHandler(CustomerApprovedException.class)
	public String handleCustomerApprovedException(CustomerApprovedException e) {
		return e.getMessage();
	}
	
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(LoanNotApproved.class)
	public String handleLoanNotApproved(LoanNotApproved e) {
		return e.getMessage();
	}
	
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(LoginRequiredException.class)
	public String handleLoginRequiredException(LoginRequiredException e) {
		return e.getMessage();
	}
	
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(NoDataToDisplayException.class)
	public String handleNoDataToDisplayException(NoDataToDisplayException e) {
		return e.getMessage();
	}
	
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(NotAccessibleException.class)
	public String handleNotAccessibleException(NotAccessibleException e) {
		return e.getMessage();
	}
	
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(PasswordAuthenticationFailedException.class)
	public String handlePasswordAuthenticationFailedException(PasswordAuthenticationFailedException e) {
		return e.getMessage();
	}
	
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(UserRegistrationException.class)
	public String handleUserRegistrationException(UserRegistrationException e) {
		return e.getMessage();
	}
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(AlreadyRegisterException.class)
	public String handleAlreadyRegisterException(AlreadyRegisterException e) {
		return e.getMessage();
	}
	
	
	
	@ResponseStatus(HttpStatus.EXPECTATION_FAILED)
	@ExceptionHandler(Exception.class)
	public String handleError(Exception e) {
		e.printStackTrace();
		return e.getMessage();
	}

}
