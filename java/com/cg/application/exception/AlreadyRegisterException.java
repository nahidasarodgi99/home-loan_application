package com.cg.application.exception;

public class AlreadyRegisterException extends RuntimeException {
	public AlreadyRegisterException(String msg) {
		super(msg);
	}

}
