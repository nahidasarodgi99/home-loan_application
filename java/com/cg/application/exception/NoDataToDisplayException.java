package com.cg.application.exception;

public class NoDataToDisplayException extends RuntimeException {

	public NoDataToDisplayException(String msg) {
		super(msg);
	}
	

}
