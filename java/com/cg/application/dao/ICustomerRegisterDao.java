package com.cg.application.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cg.application.entity.UserDetails;
@Repository
public interface ICustomerRegisterDao extends JpaRepository<UserDetails,String>{
	public UserDetails findByUsername(String name);

}
