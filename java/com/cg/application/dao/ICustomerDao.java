package com.cg.application.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cg.application.entity.Customer;
import com.cg.application.entity.Loan;

@Repository
public interface ICustomerDao extends JpaRepository<Customer, Integer>{
	Loan save(Loan loan);
	

}
