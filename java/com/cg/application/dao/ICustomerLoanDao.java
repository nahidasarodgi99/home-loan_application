package com.cg.application.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cg.application.entity.Loan;


public interface ICustomerLoanDao extends JpaRepository<Loan, Integer>{
	
	@Query("from Loan where custid=:id")
	List<Loan> findByCustId( @Param("id") int id);
	
	@Query("from Loan where custid=:id and applicationid=:appid")
	Optional<Loan> findByCustIdAppId( @Param("id") int id, @Param("appid") int appid);
	
	
	
	
	
	

}
