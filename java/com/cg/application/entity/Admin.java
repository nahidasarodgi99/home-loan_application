package com.cg.application.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "admin_detail")
public class Admin {
	@Id
	private String username;
	private String password;
	private String role;
	
	
	public Admin() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Admin(String username, String password, String role) {
		super();
		this.username = username;
		this.password = password;
		this.role = role;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getRole() {
		return role;
	}


	public void setUserRole(String role) {
		this.role = role;
	}


	@Override
	public String toString() {
		return "UserDetails [username=" + username + ", password=" + password + ", userRole=" + role + "]";
	}
	



}
