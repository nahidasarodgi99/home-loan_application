package com.cg.application.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.cg.application.entity.Customer;

@Component
public class CustomerUtil {
	public Customer toDetails(Customer customer) {
		return new Customer(customer.getCustId(), customer.getCustomerName(), customer.getMobileNumber(), 
					customer.getEmail(), customer.getDateOfBirth(), customer.getGender(), customer.getNationality(), 
					customer.getAadharNumber(), customer.getPanNumber());
	}

	public List<Customer> toDetails(List<Customer> customers) {
		List<Customer> custList = new ArrayList<>();
		for (Customer customer: customers) {
			Customer details = toDetails(customer);
			custList.add(details);
		}
		return custList;
	}
	

}
